$(document).ready(function(){

	$("#sign-in").click(function(){
		$("#login-dropdown").slideDown('slow');
		$("body").append('<div class="bg-black" onclick="javascript: hellow()"></div>');		
	});


	$("#payment").submit();

});

function hellow(){
	$(".bg-black").fadeOut(1000);	
	$("#login-dropdown").slideUp('slow');
	$(".bg-black").remove();
}
var BASE_URL = 'http://'+window.location.hostname+'/ewayenglish/';
$('form[name="feedback_form"]').submit(
	function(event){
		event.preventDefault();
		var url = BASE_URL+'external/public_feedback';
		$.ajax({
			url: url,
			type: 'post',
			data: $('form[name="feedback_form"]').serialize(),
			success: function(data){
				var res = $.parseJSON(data);
				if(res.stat=='success'){
					document.feedback_form.reset();
				}
				$('#feedbackerror').html(res.msg);
			}
		}).error(function(){
			alert('Connection error');
		});		
	}
	
);

$('form[name="signup_form"]').submit(function(event){
			
	event.preventDefault();
	var url = BASE_URL+'external/login_validation';
	$.ajax({
		url: url,
		type: 'post',
		data: $('form[name="signup_form"]').serialize(),
		success: function(data){
			var result = $.parseJSON(data);
			if(result.status=='success'){
				var page = '';
				switch(result.message){
					case '1':
						
						page = 'admin';
						break;
					case '2':
						
						page='teachers';
						break;
					case '3':
						
						page='students';
						break;
				}
				document.location.href = BASE_URL+'dashboard/'+page;
			}
			$('#loginerror').html(result.message);

		}
	}).error(function(){
		alert('Connection error');
	});		
});


function teacher_selected(id){
	var url = BASE_URL+'external/get_teacher/'+id;

	$.get( 
		url, 
		function(data) {
			var result = $.parseJSON(data);
  			// $("#teachers-slider");
  			$("#teachers-slider #exp").html(result.start_teaching);
  			$("#teachers-slider #ed_bg").html(result.educational_background);
  			$("#teachers-slider #tc_motto").html(result.motto);
  			$("#tc_prof_img").attr('src',BASE_URL+'assets/avatars/teachers/'+result.profile_image);
  			// alert( data );
		}
	);
}