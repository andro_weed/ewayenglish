/*09/30/13 : rex*/
var BASE_URL = 'http://'+window.location.hostname+'/ewayenglish/';
$('form[name="settingsform"]').submit(
	function(event){
		event.preventDefault();
		var url = BASE_URL+'dashboard/savesettings';
		$.ajax({
			url: url,
			type: 'post',
			data: $('form[name="settingsform"]').serialize(),
			success: function(data){
				var result = $.parseJSON(data);
				$('input[type="text"]').css('border','1px solid #d6d5d5');
				if(result.status=='Success'){
					alert('Success');
				}else{
					$.each(result.msg,function(){
						$('input[name="'+this+'"]').css('border','1px solid red');
					});
				}
			}
		});
	}
);
$('input[name="changepassword"]').click(function(){
	if($(this).prop('checked')){
		$('.passwordcont').fadeIn(function(){
			$('.passwordcont').css('display','block');
		});
		
	}else{
		$('.passwordcont').fadeOut(function(){
			$('.passwordcont').css('display','none');
		});
	}
});