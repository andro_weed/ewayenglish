<?php

/**
* 
*/
class Comingsoon extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('subscribers_model');
	}

	function index()
	{

		$this->load->library('form_validation');

		$this->form_validation->set_error_delimiters('<p style="color:red;text-align: center;padding-bottom: 10px;">', '</p>');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[subscribers.email]');
		$this->form_validation->set_message('is_unique', 'Your already subscribed using this email address.');
		$this->form_validation->set_message('valid_email', 'Your must provide a valid email address to subscribe.');
		if($this->form_validation->run() == FALSE)
		{

			$this->load->view('comingsoon/comingsoon_view');

		}else{

			$email = $this->input->post('email');
			
			$data = array('email'=>$email);

			$success = $this->subscribers_model->save_email($data);
			
			if($success)
			{

			}
			redirect(base_url('comingsoon?success=1'));
		}
	}

	function email()
	{
		$this->load->library('email');

		$this->email->from('your@example.com', 'Your Name');
		$this->email->to('someone@example.com'); 
		$this->email->cc('another@another-example.com'); 
		$this->email->bcc('them@their-example.com'); 

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');	

		$this->email->send();

		echo $this->email->print_debugger();
	}	

}