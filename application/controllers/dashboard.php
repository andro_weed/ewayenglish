<?php

/**
* 
*/
class Dashboard extends CI_Controller
{
	
	var $tpl = 'dashboard/template';
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_log_in')) redirect(base_url());
		
		$this->load->model('users_model');

	}
	function index()
	{
		switch (get_access($this->session->userdata('uid'))) {
			case '1':
				echo "under contruction";
				break;
			case '2':
				redirect(base_url('dashboard/teachers'));
				break;
			case '3':
				redirect(base_url('dashboard/students'));
				break;
			default:
				redirect(base_url());
				break;
		}
	}
	function teachers()
	{
		if(get_access($this->session->userdata('uid'))!=2) redirect(base_url('dashboard'));
		$prefs = array (
               'start_day'    => 'sunday',
               'month_type'   => 'long',
               'day_type'     => 'short',
               'show_next_prev'     => true
             );
		$prefs['template'] = '

		   {table_open}<table border="0" cellpadding="0" cellspacing="0">{/table_open}

		   {heading_row_start}<thead><tr>{/heading_row_start}

		   {heading_previous_cell}<th><a href="'.base_url('dashboard/teachers').'/{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
		   {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
		   {heading_next_cell}<th><a href="'.base_url('dashboard/teachers').'/{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

		   {heading_row_end}</tr></thead>{/heading_row_end}

		   {week_row_start}<tr class="weeks">{/week_row_start}
		   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr class="dates">{/cal_row_start}
		   {cal_cell_start}<td>{/cal_cell_start}

		   {cal_cell_content}<a href="{content}">{day}</a>{/cal_cell_content}
		   {cal_cell_content_today}<div class="highlight"><a href="{content}">{day}</a></div>{/cal_cell_content_today}

		   {cal_cell_no_content}<p>{day}</p><a class="assign-schedule" data-toggle="modal" href="#sched-modal" class="btn btn-primary btn-lg">3 classes</a>{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

		   {cal_cell_blank}&nbsp;{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}
		';
		$this->load->library('calendar',$prefs);
	
       

		$data['calendar'] =  $this->calendar->generate($this->uri->segment(3), $this->uri->segment(4));
		
		$this->template_lib->set_view($this->tpl, 'dashboard/teachers_view',$data);
	}

	function students($page='materials')
	{
		if(get_access($this->session->userdata('uid'))!=3) redirect(base_url('dashboard'));
		$data['page'] = $page;
		$data['page_content'] = '';
		switch ($page) {
			case 'materials':
				$data['page_content'] = $this->load->view('dashboard/students/materials','',TRUE);
				break;
			case 'teacher':
				$prefs = array (
		               'start_day'    => 'sunday',
		               'month_type'   => 'long',
		               'day_type'     => 'short',
		               'show_next_prev'     => true
		             );
				$prefs['template'] = '

				   {table_open}<table border="0" cellpadding="0" cellspacing="0">{/table_open}

				   {heading_row_start}<thead><tr>{/heading_row_start}

				   {heading_previous_cell}<th><a href="'.base_url('dashboard/students/teacher').'/{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
				   {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
				   {heading_next_cell}<th><a href="'.base_url('dashboard/students/teacher').'/{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

				   {heading_row_end}</tr></thead>{/heading_row_end}

				   {week_row_start}<tr class="weeks">{/week_row_start}
				   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
				   {week_row_end}</tr>{/week_row_end}

				   {cal_row_start}<tr class="dates">{/cal_row_start}
				   {cal_cell_start}<td>{/cal_cell_start}

				   {cal_cell_content}<a href="{content}">{day}</a>{/cal_cell_content}
				   {cal_cell_content_today}<div class="highlight"><a href="{content}">{day}</a></div>{/cal_cell_content_today}

				   {cal_cell_no_content}<p>{day}</p><a class="assign-schedule" data-toggle="modal" href="#sched-modal" class="btn btn-primary btn-lg">3 classes</a>{/cal_cell_no_content}
				   {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

				   {cal_cell_blank}&nbsp;{/cal_cell_blank}

				   {cal_cell_end}</td>{/cal_cell_end}
				   {cal_row_end}</tr>{/cal_row_end}

				   {table_close}</table>{/table_close}
				';
				$this->load->library('calendar',$prefs);
			
		       

				$data['calendar'] =  $this->calendar->generate($this->uri->segment(4), $this->uri->segment(5));
				$data['page_content'] = $this->load->view('dashboard/students/teacher',$data,TRUE);
				break;
			case 'inbox':
				$data['page_content'] = $this->load->view('dashboard/students/inbox','',TRUE);
				break;
			case 'settings':
				$this->settings_setter($data);
				$data['page_content'] = $this->load->view('dashboard/students/settings',$data,TRUE);
				break;
			default:
				# code...
				break;
		}


		
		$this->template_lib->set_view($this->tpl, 'dashboard/students_view',$data);
	}
	function settings_setter(&$data){
		$uid = $this->session->userdata('uid');
		$data['info'] = $this->users_model->get_user_info($uid);
	}
	function savesettings(){
		$this->load->library('form_validation');
		$post = $this->input->post();

		$this->form_validation->set_rules('username','username','required|trim|xss_clean|callback_validateusername');
		$this->form_validation->set_rules('firstname','firstname','required|trim|xss_clean');
		$this->form_validation->set_rules('lastname','lastname','required|trim|xss_clean');
		$this->form_validation->set_rules('skypename','skypename','required|trim|xss_clean');
		$this->form_validation->set_rules('email','email','required|trim|xss_clean|valid_email');
		$this->form_validation->set_rules('cover','cover','required|trim|xss_clean');
		if(isset($post['changepassword'])){
			$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[confpassword]|md5');
			$this->form_validation->set_rules('confpassword', 'Password Confirmation', 'trim|required');
		}
		
		$stat = 'Success';
		$msg = array();
		$uid = $this->session->userdata('uid');
		if($this->form_validation->run()){
				$id = $this->session->userdata('uid');
				 $data = array(
				   'firstname' => $post['firstname'] ,
				   'lastname' => $post['lastname'] ,
				   'skypename' => $post['skypename'] , 
				   'email' => $post['email'] ,
				   'username' => $post['username'] ,
				   'quote' => $post['cover']
				);
				 
				if(isset($post['changepassword'])){ 
					$data['password'] = md5($post['password']); 
				}
					
				$this->users_model->save_settings($data,$id);
		}else{
			foreach($post as $key=>$val){
				$err = form_error($key);
				if(!empty($err)){
					$msg[] = $key;
				}
			}
			$stat = 'Failed';
		}
		echo  json_encode(array(
							'status'=>$stat,
							'msg' => $msg
						));
	}
	function validateusername($uname){
		$uid = $this->session->userdata('uid');
		if(!$this->users_model->isusernameexists($uid,$uname)){
			return true;
		}else{
			$this->form_validation->set_message('validateusername','"'.$value.'" already exists');
			return false;
		}
	}

}