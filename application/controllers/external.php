<?php

/**
* 
*/
class External extends CI_Controller			
{
	
	var $tpl = 'external_pages/template';
	function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
		$this->load->library('form_validation');
	}

	function test()
	{
		echo '<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="63AADTSW7JAWG">
				<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
				</form>
'
				;
	}

	
	function index($error=false)
	{
		$data['error'] = $error;
		$this->set_homedata($data);
		$this->template_lib->set_view($this->tpl, 'external_pages/main_view',$data);

		
	}
	function set_homedata(&$data){
		$this->load->model('notification_model');
		$notify = $this->notification_model->collect();
		$data['notify'] = '';
		foreach($notify as $val){
			$data['notify'] .= '<tr><td>'.$val['notificationdescription']. '<br/>' . $val['notificationdate'] . '</td></tr>';
		}
	}

	function pricing()
	{
		$this->template_lib->set_view($this->tpl, 'external_pages/pricing_view');
	}
	function signup()
	{
		$this->template_lib->set_view($this->tpl, 'external_pages/signup_view');
	}
	function selection()
	{
		$this->template_lib->set_view($this->tpl, 'external_pages/selection_view');
	}
	function aboutus()
	{
		$this->template_lib->set_view($this->tpl, 'external_pages/aboutus_view');
	}
	
	function faqs()
	{
		$this->template_lib->set_view($this->tpl, 'external_pages/faqs_view');
	}
	function materials()
	{
		$this->template_lib->set_view($this->tpl, 'external_pages/material_view');
	}

	function thankyou()
	{
		$this->template_lib->set_view($this->tpl, 'external_pages/thankyou_view');
	}

	function login_validation(){
		$this->form_validation->set_rules('username','Username','required|trim|callback_validate_credentials');
		$this->form_validation->set_rules('password','Password','required|md5|trim');
		// $this->form_validation->set_error_delimiters('<div style="text-align:center">', '</div>');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$status = 'success';
		$message = '';
		if($this->form_validation->run()){			
			$data = $this->users_model->get_log_information($username,$password);
			$data = $data[0];
			$sess_data = array(
				'uid'		=> $data->id,
				'is_log_in'	=> true
				);
			$this->session->set_userdata($sess_data);
			$message = $data->access;
		
		}else{
			$status = 'failed';
			$message = '<div style="border:1px solid #f98dae;padding:2px;text-align:center;color:#fff;background-color:#fe9dbb;">Sign up failed</div>';
		}

		echo json_encode(array('status'=>$status,'message'=>$message));
	}
	public function validate_credentials(){
		
		if($this->users_model->can_log_in()){
			return true;
		}else{
			$this->form_validation->set_message('validate_credentials','Incorrect username/password');
			return false;
		}
	}
	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
	public function validate_signup(){
		$this->form_validation->set_rules('firstname','Firstname','required|trim');
		$this->form_validation->set_rules('lastname','Lastname','required|trim');
		$this->form_validation->set_rules('skypename','Skypename','required|trim');
		$this->form_validation->set_rules('email','Email','required|trim|xss_clean|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('birthday','Birthday','required|trim|xss_clean');
		$this->form_validation->set_rules('username','Username','required|trim|is_unique[users.username]');
		$this->form_validation->set_rules('password','Password','required|trim|min_length[6]|matches[confirmpassword]|md5');
		$this->form_validation->set_rules('confirmpassword','Confirmpassword','required|md5|trim');
		$this->form_validation->set_error_delimiters('<span>','</span>');
		$this->form_validation->set_message('is_unique','This %s is already taken.');
		$this->form_validation->set_message('matches','Password did not match.');
		$this->form_validation->set_message('min_length','Password must be at least 6 characters.');
		$this->form_validation->set_message('valid_email','Email address is not valid.');

		$post = $this->input->post();
		if($this->form_validation->run()){
			$this->users_model->insert_user($post);

			$data = $this->users_model->get_log_information($post['username'],$post['password']);
			$data = $data[0];
			$sess_data = array(
				'uid'		=> $data->id,
				'is_log_in'	=> true
				);
			$this->session->set_userdata($sess_data);

			redirect('selection');
		}else{

			$this->signup();

		}
	}


	function public_feedback(){
		$this->load->model('feedback_model');
		$this->form_validation->set_rules('email','Email','required|trim|xss_clean|valid_email');
		$this->form_validation->set_rules('uname','Name','required|trim|xss_clean');
		$this->form_validation->set_rules('message','Message','required|trim|xss_clean');
		$stat = 'success';
		$msg = '<div style="border:1px solid #78e648;padding:2px;text-align:center;color:#fff;background-color:#9cf575;">Sent!</div>';
		if($this->form_validation->run()){
			$post = $this->input->post();
			$data = array(
					'email' => $post['email'],
					'name'  => $post['uname'],
					'message' => $post['message']
				);
			$this->feedback_model->save_feedback($data);
		}else{
			$stat = 'failed';
			$msg = '<div style="border:1px solid #f98dae;padding:2px;text-align:center;color:#fff;background-color:#fe9dbb;">Feedback failed to send</div>';
		}

		echo json_encode(array('stat'=>$stat,'msg'=>$msg));

	}

	function testx()
	{
		echo '<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="hosted_button_id" value="EX9YLA5398QGN">
			<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
			';
	}

	function get_teacher($id='')
	{		
		$data = $this->users_model->get_teacher_profile($id);
		$data = $data[0];
		$data->start_teaching = calculate_work_experience($data->start_teaching);
		// printA($data);
		// exit();
		echo json_encode($data);
	}
	
}