<?php
class Users_model extends CI_Model {

	public function can_log_in(){
		$this->db->where('username',$this->input->post('username'));
		$this->db->where('password',md5($this->input->post('password')));

		$query = $this->db->get('users');
		
		if($query->num_rows() == 1){
			return true;
		}else{
			return false;
		}
	}

	public function get_log_information($username,$password){
		$this->db->select('id,access');
		$this->db->where('username',$username);
		$this->db->where('password',md5($password));
		$query = $this->db->get('users');

		return $query->result();
	}

	public function get($select = '*',$where = array())
	{
		$this->db->select($select);
		if($where) $this->db->where($where);		
		$query = $this->db->get('users');

		return $query->result();
	}
	public function insert_user($post){
       $data = array(
	   'firstname' => $post['firstname'] ,
	   'lastname' => $post['lastname'] ,
	   'skypename' => $post['skypename'] ,
	   'email' => $post['email'] ,
	   'username' => $post['username'] ,
	   'password' => md5($post['password']) ,
	   'access' => 3,
		);
		
		$query = $this->db->insert('users', $data); 

		if($query->num_rows > 0){

		}
	}
	function save_settings($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('users',$data);
		return $this->db->affected_rows();
	}
	function get_user_info($uid){
		$this->db->where('id',$uid);
		$query = $this->db->get('users');
		return $query->row_array();
	}
	function isusernameexists($id,$uname){
		$this->db->where('id !=',$id);
		$this->db->where('username',$uname);
		$query = $this->db->get('users');
		return ($query->num_rows > 0)? true : false;
	}	

	function get_teacher_profile($id='')
	{
		$this->db->select('A.id,A.firstname,A.lastname,B.educational_background,B.motto,B.start_teaching,B.profile_image');	
		$this->db->from('users as A');
		$this->db->join('teachers_profile as B','A.id=B.user_id','left');
		$this->db->where('A.access',2);
		if($id){
			$this->db->where('A.id',$id);
		}

		$query = $this->db->get();
		return $query->result();
	}

}