<?php
/**
* 
*/
class Feedback_model extends CI_Model
{
	var $main_table = 'feedbacks';

	function save_feedback($data)
	{
		$this->db->insert($this->main_table,$data);
		return $this->db->affected_rows();
	}
}