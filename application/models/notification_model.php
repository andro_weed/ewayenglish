<?php
class Notification_model extends CI_Model {
	public function collect(){
		$this->db->select('*');
		$this->db->order_by('notificationdate','desc');
		$this->db->limit(5);
		$query = $this->db->get('notification');
		return $query->result_array();

	}
}
?>
