<?php
if(!function_exists("session_checker")){
	function session_checker(){
		$ci=& get_instance();
		if(!$ci->session->userdata('is_log_in')){
				redirect('login');
				exit;
		}
	}
}


function calculate_work_experience($date)
{
	$date1 = $date; 
	$date2 = date('Y-m-d'); 

	$diff = abs(strtotime($date2) - strtotime($date1)); 
	$years   = floor($diff / (365*60*60*24)); 
	$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 

	$exp = '';
	if($years != 0){
		$yrs = $years < 2 ? ' year ' : ' years ';
		$exp .=  $years.$yrs;
		if($months != 0){
			$mnts = $months < 2 ? ' month ' : ' months ';
			$exp .=  ' and '.$months.$mnts;
		}
	}else{
		if($months != 0){
			$mnts = $months < 2 ? ' month ' : ' months ';
			$exp .=  $months.$mnts;
		}else{
			$exp .= 'no experience';
		}
	}
	
	return $exp;
}