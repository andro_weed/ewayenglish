<?php

function printA($data)
{
	echo "<pre>";
	print_r($data);
	echo "</pre>";
}

function get_fullname($id){
	$ci=& get_instance();
	$ci->load->model('users_model');
	$data = $ci->users_model->get('firstname,lastname',$data = array('id'=>$id));
	$data = $data[0];

	return $data->firstname.' '.$data->lastname;
}

function get_access($id){
	$ci=& get_instance();
	$ci->load->model('users_model');
	$data = $ci->users_model->get('access',$data = array('id'=>$id));
	$data = $data[0];

	return $data->access;
}

function get_username($id){
	$ci=& get_instance();
	$ci->load->model('users_model');
	$data = $ci->users_model->get('username',$data = array('id'=>$id));
	$data = $data[0];

	return $data->username;
}

function teacher_info($id='')
{
	$ci=& get_instance();
	$ci->load->model('users_model');
	$data = $ci->users_model->get_teacher_profile($id);

	return $data;
}