<script type="text/javascript" src="<?php echo base_url('assets/audio-player/audio-player.js');?>"></script>  
<script type="text/javascript">  
    AudioPlayer.setup("<?php echo base_url('assets/audio-player/player.swf');?>", {  
        width: 170 
    });  
</script>  
<div class="dasboard-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-2 left-dash-wrapper">
					<div class="profile-image-container">
						<img src="<?php echo base_url('assets/dashboard/img/default-profile.png');?>">
					</div>
					<div class="title-cont"><?php echo get_username($this->session->userdata('uid'));?></div>
					<table>
						<thead>
							<tr>
								<th colspan="2"><?php echo ucwords(get_fullname($this->session->userdata('uid')));?></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Age : </td>
								<td>17</td>
							</tr>
							<tr>
								<td>Experience: </td>
								<td>3 years english teacher</td>
							</tr>
						</tbody>
					</table>
					<div class="additional-inf">
						<p>Lorenempsum matica libro libradilla</p>
						<p id="audioplayer_1">Alternative content</p>  
				        <script type="text/javascript">  
				        AudioPlayer.embed("audioplayer_1", {soundFile: "<?php echo base_url('assets/introductions/faith.mp3');?>"});  
				        </script> 
					</div>
					<div class="teacher-ribbon"><p>99%</p></div>
				</div>
				<div class="col-md-10 right-dash-wrapper">
						<div class="welcome-message">
							<p>I'm starting to learn<br/> a better english.</p>
						</div>
						<div class="calendar-container">
							<?php echo $calendar;?>
							<div class="cal-design"></div>
						</div>
				</div>
				
			</div>
		</div>
	</div>