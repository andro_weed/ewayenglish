<!DOCTYPE html>
<html>
<head>
	<title>Landing Page</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap3/dist/css/bootstrap.min.css');?>">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/external/bootstrap3/dist/css/bootstrap-theme.css');?>"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/external/bootstrap3/dist/css/bootstrap.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dashboard/css/dashboard.css');?>">
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/external/css/datepicker.css');?>">
	<style type="text/css">
		.datepicker{
			z-index: 1050 !important;	
		}
	</style>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url('assets/external/bootstrap3/assets/js/html5shiv.js');?>"></script>
      <script src="<?php echo base_url('assets/external/bootstrap3/assets/js/respond.min.js');?>"></script>
    <![endif]-->
</head>
<body>

	<div class="header-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-3 logo">
					<a href="<?php echo base_url()?>"><img class="image-responsive" src="<?php echo base_url('assets/external/img/logo.png');?>"></a>
				</div>
				<div class="col-md-6 navigation">
					<ul>
						<li><a href="<?php echo base_url()?>">home</a></li>
						<li><a href="#">lessons</a></li>
						<li><a href="#">teachers</a></li>
						<li><a href="#">materials</a></li>
						<li><a href="<?php echo base_url('pricing')?>">rates &amp; payments</a></li>
					</ul>
				</div>
				<div class="col-md-3 pull-right cta-buttons">
					<a href="<?php echo base_url('logout')?>"  class="logout">logout</a>
					
				</div>
			</div>
		</div>
	</div>

	<?php echo $contents;?>




	<div class="footer-links">
		<div class="container">
			<div class="row">
				
					<div class="col-md-3">
						<p class="copy">&copy; ewayenglish.com</p>					
					</div>
					<div class="col-md-6">
						<ul class="navigation">
							<li><a href="<?php echo base_url()?>">home</a></li>
							<li><a href="#">lessons</a></li>
							<li><a href="#">teachers</a></li>
							<li><a href="#">materials</a></li>
							<li><a href="<?php echo base_url('pricing')?>">rates &amp; payments</a></li>
						</ul>
					</div>
				
					<div class="col-md-3">
						<div class="icons">
							<img class="image-responsive " src="<?php echo base_url('assets/external/img/twitter.png');?>">
							<img class="image-responsive " src="<?php echo base_url('assets/external/img/facebook.png');?>">
							<img class="image-responsive " src="<?php echo base_url('assets/external/img/skype.png');?>">
							<img class="image-responsive " src="<?php echo base_url('assets/external/img/google.png');?>">
						</div>
					</div>
			
			</div>
		</div>
	</div>
	<div id="pick-sched-modal"  class="modal fade sched-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">schedule class</h4>
	        </div>
	        <div class="modal-body">
	        	<div class="time-tracker">
		        	<p class="titles">Date</p>
		          	<input class="date-pck" id="datepicker" type="text">
		          	<p class="titles">Available time</p>
		          	<div class="date-picks">
		          		<div class="radio">
						  <label>
						    <input type="radio" name="optionsRadios" value="option2">
						    12:00 PM - 12:25 PM
						  </label>
						</div>
		          		<div class="radio">
						  <label>
						    <input type="radio" name="optionsRadios"  value="option2">
						    12:00 PM - 12:25 PM
						  </label>
						</div>
						<div class="radio">
						  <label>
						    <input type="radio" name="optionsRadios" value="option2">
						    12:00 PM - 12:25 PM
						  </label>
						</div>
		          	</div>
		        </div>
	        </div>
	        
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div>


	<div id="sched-modal"  class="modal fade sched-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">FRIDAY, August 1, 2013</h4>
	        </div>
	        <div class="modal-body">
	          <table>
	          	<tr>
	          		<td>12 <br/> AM</td>
	          		<td><div class="teachers-sched frame" data-html="true" data-toggle="tooltip" data-placement="bottom" title="<b class='done'><img src='<?php echo base_url('assets/dashboard/img/check.png');?>'/>done</b> <b class='percent'><img src='<?php echo base_url('assets/dashboard/img/star.png');?>'/>98%</b>" data-original-title="<span class='done'>done</span> <span class='percent'>98%</span>"><h6>Kung Faw</h6><p>12:00 - 12:25</p><div></td>
	          	</tr>
	          	<tr>
	          		<td>1 <br/> AM</td>
	          		<td><div class="frame"><h6>Kung Tae</h6><p>12:00 - 12:25</p><div></td>
	          	</tr>
	          	<tr>
	          		<td>2 <br/> AM</td>
	          		<td><div class="frame"><h6>Kung Lao</h6><p>12:00 - 12:25</p><div></td>
	          	</tr>
	          	<tr>
	          		<td>3 <br/> AM</td>
	          		<td><div class="frame"><h6>Kung Gaw</h6><p>12:00 - 12:25</p><div></td>
	          	</tr>
	          	<tr>
	          		<td>4 <br/> AM</td>
	          		<td><div class="frame"><h6>Kug Tong</h6><p>12:00 - 12:25</p><div></td>
	          	</tr>
	          		<tr>
	          		<td>5 <br/> AM</td>
	          		<td><div class="frame"><h6>Kauu Sing</h6><p>12:00 - 12:25</p><div></td>
	          	</tr>	
	          	<tr>
	          		<td>6 <br/> AM</td>
	          		<td><div class="frame"><h6>Liam Pou</h6><p>12:00 - 12:25</p><div></td>
	          	</tr>
	          </table>
	        </div>
	        
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div>









	<script type="text/javascript" src="<?php echo base_url('assets/external/js/jquery.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap3/dist/js/bootstrap.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/dashboard/js/jquery-ui-1.10.3.custom.min.js');?>"></script>	
	<script type="text/javascript" src="<?php echo base_url('assets/dashboard/js/dashboard.js');?>"></script>	 	
	<script type="text/javascript" src="<?php echo base_url('assets/external/js/bootstrap-datepicker.js');?>"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		
		$('.upcoming,.previous,.teachers-sched').tooltip();
		$( "#materials" ).accordion();
		$('#datepicker').datepicker();
	});
	</script>
</body>
</html>