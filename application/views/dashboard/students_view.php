<script type="text/javascript" src="<?php echo base_url('assets/audio-player/audio-player.js');?>"></script>  
<script type="text/javascript">  
    AudioPlayer.setup("<?php echo base_url('assets/audio-player/player.swf');?>", {  
        width: 270 
    });  
</script> 
<div class="dasboard-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-2 left-dash-wrapper">
					<div class="profile-image-container">
						<img src="<?php echo base_url('assets/dashboard/img/default-profile.png');?>">
					</div>
					<div class="title-cont"><?php echo get_username($this->session->userdata('uid'));?></div>
					<div class="legend-cont">
						<ul>
							<li>&nbsp;&nbsp;&nbsp;upcoming</li>
							<li>&nbsp;&nbsp;&nbsp;previous</li>
						</ul>
					</div>
					<div class="classes-cont">
						<h5>classes</h5>
						<p class="upcoming"  data-toggle="tooltip" data-placement="right" title="Rex Cambarijan" data-original-title="Rex Cambarijan">
							Sep. 12,2013 <br>
							8:30 am-5:40 pm
						</p>
						<p class="upcoming" data-toggle="tooltip" data-placement="right" title="Rex Cambarijan" data-original-title="Rex Cambarijan">
							Sep. 12,2013 <br>
							8:30 am-5:40 pm
						</p>
						<p class="previous" data-toggle="tooltip" data-placement="right" title="Rex Cambarijan" data-original-title="Rex Cambarijan">
							Sep. 12,2013 <br>
							8:30 am-5:40 pm
						</p>
						<p class="previous" data-toggle="tooltip" data-placement="right" title="Rex Cambarijan" data-original-title="Rex Cambarijan">
							Sep. 12,2013 <br>
							8:30 am-5:40 pm
						</p>
						<p class="previous" data-toggle="tooltip" data-placement="right" title="Rex Cambarijan" data-original-title="Rex Cambarijan">
							Sep. 12,2013 <br>
							8:30 am-5:40 pm
						</p>
						<p class="previous" data-toggle="tooltip" data-placement="right" title="Rex Cambarijan" data-original-title="Rex Cambarijan">
							Sep. 12,2013 <br>
							8:30 am-5:40 pm
						</p>
					</div>
				</div>
				<div class="col-md-10 right-dash-wrapper">
						<div class="welcome-message">
							<p>I'm starting to learn<br/> a better english.</p>
							<div class="navigations">
								<ul>
									<?php
										$pages = array('teacher','materials','inbox','settings');
										
										foreach($pages as $value){
									?>
										<li <?php echo ($value==$page) ? 'class="selected"': '';?>><a  href="<?php echo base_url('dashboard/students/'.$value);?>"><?php echo $value;?></a></li>
										
									<?php }?>
								</ul>
							</div>
						</div>
						<?php echo $page_content;?>
				</div>
			</div>
		</div>
	</div>