<!-- materials view -->
	<div class="materials" id="materials">
		 <h3>Standard Materials</h3>
		 <div class="cont">	
		 	<div class="row">
			   		<div class="col-md-10 col-md-offset-1">
			   			<div class="row">
			   				<div class="col-md-4">
			   					<ul>
					   				<li><h4>Reading</h4></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   			</ul>
			   				</div>
			   				<div class="col-md-4">
			   					<ul>
					   				<li><h4>Reading</h4></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   			</ul>
			   				</div>
			   				<div class="col-md-4">
			   					<ul>
					   				<li><h4>News</h4></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   			</ul>
			   				</div>
			   			</div>
			   			
			   		</div>
			  </div>
		  </div>
		  <h3>Childrens Materials</h3>
			  <div class="cont">
			    <div class="row">
			   		<div class="col-md-10 col-md-offset-1">
			   			<div class="row">
			   				<div class="col-md-4">
			   					<ul>
					   				<li><h4>Reading</h4></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   			</ul>
			   				</div>
			   				<div class="col-md-4">
			   					<ul>
					   				<li><h4>Reading</h4></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   			</ul>
			   				</div>
			   				<div class="col-md-4">
			   					<ul>
					   				<li><h4>News</h4></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   			</ul>
			   				</div>
			   			</div>
			   			
			   		</div>
			  </div>
			  </div>
		  <h3>Business Materials</h3>
			  <div class="cont">
			    <div class="row">
			   		<div class="col-md-10 col-md-offset-1">
			   			<div class="row">
			   				<div class="col-md-4">
			   					<ul>
					   				<li><h4>Reading</h4></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   			</ul>
			   				</div>
			   				<div class="col-md-4">
			   					<ul>
					   				<li><h4>Reading</h4></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   			</ul>
			   				</div>
			   				<div class="col-md-4">
			   					<ul>
					   				<li><h4>News</h4></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   				<li><p>the quick brown fox</p></li>
					   			</ul>
			   				</div>
			   			</div>
			   			
			   		</div>
			  	</div>
			</div>					  
		  
		<a class="more-info" href="#">access more materials</a>
	</div>

	<!-- end materials -->