<div class="settings">
	<div class="row">
		<div class="col-md-3 upload">
			<div class="upload-preview"></div>
			<button class="upload-btn">upload</button>
		</div>
		<div class="col-md-8 update-form">
			<form name="settingsform">
				<div class="row">
					<div class="col-md-12">
						<p>Cover qoute</p>
						<input type="text" name="cover" class="qoute" value="<?php echo $info['quote']?>" >	
					</div>
					<div class="clearfix"></div>
					<div class="col-md-6">
						<p>username</p>
						<input type="text" name="username" value="<?php echo $info['username']?>">
					</div>				
					<div class="col-md-6">
						<p style="color: #0d92e5;">skype name</p>
						<input type="text" name="skypename" value="<?php echo $info['skypename']?>">
						<p style="color: #0d92e5;">email</p>
						<input type="text" name="email" value="<?php echo $info['email']?>">
					</div>	
					<div class="clearfix"></div>
					<div class="col-md-12">
						<label><input type="checkbox" name="changepassword" value="1"/>&nbsp;&nbsp;&nbsp;change password</label>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-6">	
						<div class="passwordcont" style="display:none">					
							<p>password</p>
							<input type="password" name="password">
							<p>confirm password</p>
							<input type="password" name="confpassword">
						</div>
					</div>
					<div class="col-md-6">						
						<p>firstname</p>
						<input type="text" name="firstname" value="<?php echo $info['firstname']?>">
						<p>lastname</p>
						<input type="text" name="lastname" value="<?php echo $info['lastname']?>">
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12">
						<button>save</button>
					</div>
				</div>
			</form>	
		</div>
	</div>
</div>