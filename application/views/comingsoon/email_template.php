<!DOCTYPE html>
<html>
	<head>
		<title></title>
	</head>
	<body style="background-color: #e3e9fd;font-family: Comic-sans">
		<div style="width: 45%;margin: 0 auto;">
			Greetings!<br/><br/><br/>

			Welcome to E-way Better English Language Institute!<br/><br/>

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We highly appreciate your interest to join and involve yourself in a new and dynamic 
			teaching and learning experience. We guarantee you a whole new and entertaining 
			experience of English language learning. Please stand by for more updates about the launching of the school. 
			We will be posting updates on the website homepage until the institute officially opens.
			<br/>
			<br/>

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In the coming days, you will be receiving updates when new materials and new schedules of classes will be available.
			<br/>
			<br/>

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Again, we thank you very much for taking the time to visit our site. Please stand by for more exciting updates!
			<br/>
			<br/>

			We are looking forward to seeing you when the school officially opens soon!
			<br/>
			<br/>
			<br/>
			Best regards,<br/>
			E-way Better English Language Institute

		</div>
	</body>
</html>


