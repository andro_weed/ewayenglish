<!DOCTYPE html>
<html lang='en'>
	<head>
		<title>Coming Soon</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	  	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/public/css');?>/bootstrap.css">
	  	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/public/css/bootstrap-responsive.css');?>">
	  	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/public/css/reset.css');?>">
	  	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/public/css/custom.css');?>">
		<link href="/themes/loangarage/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

	</head>
	<body>


		<div class="coming-header">
			<div class="container">
				<div class="row">
					<div class="span10 offset1 content">
						<h4>welcome to</h4>
						<h1>E-way Better English</h1>
						<p>
							Were coming soon!<br>
							Thank you ver much for visiting E-way. <br><br>

							E-way Better English is a new online English School that offers a dynamic and sophisticated<br>
							learning experience to every english learner through the use of standardized materials<br>
							especially designed by English-language professionals. <br><br>

							Lets learn English together! Subscribe now for more personalized updates, and see you in class real soon! :)
						</p>
					</div>

					<div class="clearfix"></div>
					<div class="span6 offset3">

						<?php echo form_open('comingsoon');?>
							<?php if(isset($_GET['success'])==1):?>
								<p style="font-weight: bold;color: green;text-align:center;">Successfully added to our mailing list.</p><br/>
							<?php endif;?>
							<?php echo validation_errors(); ?>
							<input type="text" value="<?php echo set_value('email'); ?>" name="email" placeholder="youremail@domain.com">
							<input type="submit" value="Subscribe">
						</form>
						
					</div>
					<div class="span12 bubble">
						<img src="<?php echo base_url('assets/public/img/comingsoon-buble.png');?>">
					</div>
				</div>
			</div>
		</div>
		<div class="coming-footer">
			<div class="container">
				<div class="row">				
					<div class="logo-cont">
						<!-- ribbon-pattern.png -->
						<div >
							<img src="<?php echo base_url('assets/public/img/comingsoon-footer.png');?>">
						</div>
					</div>
				</div>
			</div>
		</div>

		
		<script type="text/javascript" src="<?php echo base_url('assets/public/js/jquery.js');?>"></script>
  		<script type="text/javascript" src="<?php echo base_url('assets/public/js/bootstrap.min.js');?>"></script>
	</body>
</html>