<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Login</title>
	</head>
	<body>
	<h1>Log-in</h1>
	<?php

		echo form_open('main/login_validation');

		echo validation_errors();

		echo '<p> Email :';
		echo form_input('email');
		echo '</p>';

		echo '<p> Password :';
		echo form_password('password');
		echo '</p>';

		echo '<p>';
		echo form_submit('login_submit','login');
		echo '</p>';

		echo form_close();
	?>
	</body>
</html>