<style type="text/css">
	
.faq-header-label{

	color: #fff; 
	font-family: kristen-regular; 
	font-size: 30px; 
	text-align: center;
	position: relative;
	bottom: 110px;

}

.faq-contents{

	margin-left: 100px;
}

.faq-content-question{

	color: #0099cc;
	font-size: 14px;
}

.faq-content-ans{

	margin-left: 30px;
	margin-top: 10px;
	color: #999999;
	font-size: 12px;
}

</style>
<div class="head-cloud-container"></div>
	<div class="container">
		<div class="row">
			<div class="signup-wrapper">
				<div class="faq-header-label">
					Frequently Ask Questiions
				</div>
			</div>
			<div class="faq-content-main">
				<div class="faq-contents">
					<div>
						<div class="faq-content-question">
							1)	What do I need to enroll in Eway?
						</div>
						<div class="faq-content-ans">
							Answer: <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									a. Skype account.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									b. Payment method:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										i.   Paypal<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										ii.  Debit or Credit Card
						</div><br/>

						<div class="faq-content-question">
							2)	Can I still create account and login to www.ewayenglish.com even if I do not have skype account?
						</div>
						<div class="faq-content-ans">
							Answer: No. Unfortunately, every student needs a Skype account to register. However, if you do not have an account, you can create a Skype account <a target="_blank" href="https://login.skype.com/account/signup-form" >here</a>.<br/>
						</div><br/>

						<div class="faq-content-question">
							3)	How to install Skype?
						</div>
						<div class="faq-content-ans">
							Answer: <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									a. Download Skype istaller <a target="_blank" href="http://www.skype.com/en/download-skype/skype-for-computer/">here</a>.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									b. Run the downloaded installer.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									c. If you do not have any account please click “Create an account” or click <a target="_blank" href="https://login.skype.com/account/signup-form" >here</a>.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									d. If you have an existing account just do sign in using your existing account.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									e. Sign in to skype and start enjoying English conversations with your teacher.
										
						</div><br/>

						<div class="faq-content-question">
							4)	I do not have any Paypal account, how could I pay online?
						</div>
						<div class="faq-content-ans">
							Answer: <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									1. You can use your debit card or credit card or any card that registered online.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									2. You can sign up for paypal here.
									
						</div><br/>

						<div class="faq-content-question">
							5)	Can I download the eway materials?
						</div>
						<div class="faq-content-ans">
							Answer: No. You can only view them.
									
						</div><br/>

						<div class="faq-content-question">
							6)	Do you offer discounts to students?
						</div>
						<div class="faq-content-ans">
							Answer: Yes. If you enroll in a 50-minute/day plan, you can save &yen;1000.
									
						</div><br/>

						<div class="faq-content-question">
							7)	I cannot login using my account.
						</div>
						<div class="faq-content-ans">
							Answer: <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									You may forgot your password. If so, please follow the steps:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										a. Click login.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										b. Click forgot password.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										c. Provide the necessary information.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									You can also edit your password or other account information in your profile page.
						</div><br/>

						<div class="faq-content-question">
							8)	Can I user free trial again?
						</div>
						<div class="faq-content-ans">
							Answer: No, you can only take advantage of the Free Trial plan once. When you subscribe, you will have to upgrade to a paid plan.
							<a href="#">Learn more.</a>
									
						</div><br/>

						<div class="faq-content-question">
							9)	Can I deactivate my account?
						</div>
						<div class="faq-content-ans">
							Answer: Answer:  Yes, you can. Click <a href="#">here</a> for details.
									
						</div><br/>

						<div class="faq-content-question">
							10)	Can I use my mobile/smart phone to access the E-way website?
						</div>
						<div class="faq-content-ans">
							Answer: Yes. Just like using a PC, you can log in to your E-way account by accessing our website (www.ewayenglish.com).			
						</div><br/>

						<div class="faq-content-question">
							11)	I cannot speak English at all, is Eway a good school for me.
						</div>
						<div class="faq-content-ans">
							Answer: Yes. Eway has professional instructors who can guide you to a very structured learning in your own pace. Eway also provides materials suited for every student’s specific needs.
						</div><br/>

						<div class="faq-content-question">
							12)	I need to improve my TOEIC/TOEFL score, can Eway help me.
						</div>
						<div class="faq-content-ans">
							Answer: Yes. In so many ways, the Eway tutors and Eway materials can provide a good training and learning environment for the students who want to improve their TOEIC/TOEFL scores.
						</div><br/>


					</div>
				</div>
			</div>
		</div>

</div>
<div class="footer-com-container"></div>
