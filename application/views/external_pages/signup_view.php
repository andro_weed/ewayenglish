<div class="head-cloud-container"></div>
<div class="signup-container">
	<div class="container">
		<div class="row">
			<div class="signup-wrapper">
				<div class="steps">
					<ul>
						<li>
							<p class="selected">1</p>
						</li>
						<li><div>&nbsp;</div></li>
						<li>
							<p>2</p>
						</li>
						<li><div>&nbsp;</div></li>
						<li>
							<p>3</p>
						</li>
					</ul>
					<ul>
						<li><h6>sign up</h6></li>						
						<li><h6 class="mid">pricing</h6></li>					
						<li><h6>completion</h6></li>
					</ul>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					<?php 
					echo form_open('external/validate_signup'); 
					?>
						<div class="col-md-4">
							<div class="register-container">
								<label>Skype name *</label>
								<span style="position: relative; left: 3px; color:#89f342; font-size: 10px;">No account? <a style="text-decoration: underline; color: #2ea7f3;" href="https://login.skype.com/account/signup-form" target="_blank" >Sign-up</a></span>
								<input style="<?php echo form_error('skypename') ? 'margin-bottom: 0px; border: 1px solid #ff9999;' : ''?>" name="skypename" value="<?php echo set_value('');?>">
								<?php echo form_error('skypename') ? '<span style="font-size: 10px; color:#ff9999;">'.form_error('skypename').'</span>' : ''?>
								<label>Firstname *</label><br/>
								<input style="<?php echo form_error('firstname') ? 'margin-bottom: 0px; border: 1px solid #ff9999;' : ''?>" type="text" name='firstname' value="<?php echo set_value('firstname');?>">
								<?php echo form_error('firstname') ? '<span style="font-size: 10px; color:#ff9999;">'.form_error('firstname').'</span>' : ''?>
								<label>Lastname *</label><br/>
								<input style="<?php echo form_error('lastname') ? 'margin-bottom: 0px; border: 1px solid #ff9999;' : ''?>" type="text" name='lastname' value="<?php echo set_value('lastname');?>">
								<?php echo form_error('lastname') ? '<span style="font-size: 10px; color:#ff9999;">'.form_error('lastname').'</span>' : ''?><br/>
								<label>Email *</label><br/>
								<input style="<?php echo form_error('email') ? 'margin-bottom: 0px; border: 1px solid #ff9999;' : ''?>" type="text" name='email' value="<?php echo set_value('email');?>">
								 <?php echo form_error('email') ? '<span style="font-size: 10px; color:#ff9999;">'.form_error('email').'</span>' : ''?>
								 
							</div>
						</div>
						<div class="col-md-4">
							<div class="register-container">
								<label>Birthday</label>	
								<input id="datepicker" style="<?php echo form_error('birthday') ? 'margin-bottom: 0px; border: 1px solid #ff9999;' : ''?>" type="text" name="birthday" value="<?php echo set_value('birthday');?>"/>
								<?php echo form_error('birthday') ? '<span style="font-size: 10px; color:#ff9999;">'.form_error('birthday').'</span>' : ''?>
								<label>Username *</label><br/>
								<input style="<?php echo form_error('username') ? 'margin-bottom: 0px; border: 1px solid #ff9999;' : ''?>" type="text" name='username' value="<?php echo set_value('username');?>">
								<?php echo form_error('username') ? '<span style="font-size: 10px; color:#ff9999;">'.form_error('username').'</span>' : ''?>
								<label>Password *</label><br/>
								<input style="<?php echo form_error('password') ? 'margin-bottom: 0px; border: 1px solid #ff9999;' : ''?>" type="password" name='password'>
								<?php echo form_error('password') ? '<span style="font-size: 10px; color:#ff9999;">'.form_error('password').'</span>' : ''?>
								<label>Confirm Password *</label><br/>
								<input style="<?php echo form_error('confirmpassword') ? 'margin-bottom: 0px; border: 1px solid #ff9999;' : ''?>" type="password" name='confirmpassword'> 
								
							</div>
						</div>
						<div class="col-md-4">
							<div class="register-container">
								<p>start learning and press the button!</p>
								<button>sign up</button>
							</div>
						</div>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="footer-com-container"></div>
