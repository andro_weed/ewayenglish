<?php
	// echo "<pre>";
 // 	print_r($this->session->all_userdata());
	// echo "<pre>";
?>
<div class="head-cloud-container"></div>
<div class="signup-container">
	<div class="container">
		<div class="row">
			<div class="signup-wrapper">
				<div class="steps">
					<ul>
						<li>
							<p class="selected">1</p>
						</li>
						<li><div>&nbsp;</div></li>
						<li>
							<p class="selected">2</p>
						</li>
						<li><div>&nbsp;</div></li>
						<li>
							<p>3</p>
						</li>
					</ul>
					<ul>
						<li><h6>sign up</h6></li>						
						<li><h6 class="mid">pricing</h6></li>					
						<li><h6>completion</h6></li>
					</ul>
				</div>
			</div>
			<div class="choice-form">
				<form action="<?php echo base_url('pay');?>" method="post">

					<div class="left-choice">
						<h1>single <br> plan</h1>
						<label><input type="radio" value="single" name="plan"/></label>
					</div>
					<div class="right-choice">
						<h1>double <br> plan</h1>
						<label><input type="radio" value="double" name="plan"/></label>
					</div>
					<div class="sub-button">
						<button type="submit">Finish</button>
					</div>
				</form>
				<div class="sub-button">
					<button onclick="document.location.href='thank-you'" name="trial_button" style="display: block;position: absolute;margin-left: -252px;margin-top: -30px;">Skip to Free Trial</button>
				</div>
			</div>
		</div>
	</div>

</div>
<div class="footer-com-container"></div>
