
	<div class="landing-slider-main-wrapper ">
		<div class="">
			<div id="carousel-slider" class="slider-wrapper carousel slide">
						
				  
				   <ol class="carousel-indicators">
				    <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-slider" data-slide-to="1"></li>
				    <li data-target="#carousel-slider" data-slide-to="2"></li>
				    <li data-target="#carousel-slider" data-slide-to="3"></li>
				  </ol>

				  <div class="carousel-inner">

					<!-- slider1 -->
				  	<div class="item  active ">	
				  		<div class="slide4">	
				  			<div class="container">
				  				
						  		<div class="col-md-8  ">						  			
						  			<h1>Why Eway Better English</h1>
						  			<p>
						  				E-way Better English is a new school that caters to all English Language learners. E-way uses only the 
						  				standardized materials used in Philippine English education system. English instructors are all language
						  				professionals to ensure excellent learning experience! Learn how Filipinos learn to speak like
						  				Native English Speakers
						  			</p>
						  			<?php echo anchor('aboutus','read more about us',array('class'=>'readmore'))?>
						  			<br>
						  			<br>
						  			<a class="button" href="<?php echo base_url('sign-up')?>">Try now!</a>
						  		</div>
				  			</div>

				  		</div>
				  	</div>

				  	<!-- slide2 -->
				  	<div class="item  ">	
				  		<div class=" slide3">	
				  			<div class="full">
					  			<div class="container">
							  		<div class="col-md-8  ">						  			
							  			<h1>WHY LEARN ENGLISH IN EWAY?</h1>
							  			<ul>
							  				<li>Enjoy 3-days of Free Trial</li>
							  				<li>Talk to Language Professionals every night</li>
							  				<li>Study in the comfort of your home at a very low price!</li>
							  				<li>Learn English according to the Philippine Education system!</li>
							  				<li>Access standardized materials specially designed by professionals!</li>
							  				<li>Enroll in E-way with a MoneyBack Guarantee.</li>
							  			</ul>
							  			<br>
							  			<br>
							  			<a class="button"href="<?php echo base_url('sign-up')?>">Try now!</a>
							  		</div>
						  	</div>
						  	</div>
				  		</div>
				  	</div>

				  	<!-- slide3 -->
				  	<div class="item   ">	
				  		<div class=" slide2">	
				  			<div class="container">
						  		<div class="col-md-5  ">						  			
						  			<h1>How to enjoy learning in E-way?</h1>
						  			<ol>
						  				<li>Sign up now to enjoy the 3-day Free Trial.</li>
						  				<li>Once you're signed up, view profile of the teachers adn choose a favorite!</li>
						  				<li>Access Standardized Materials</li>
						  				<li>Take the Level Test to know your English Level.</li>
						  				<li>And Enjoy learning English Everyday!</li>
						  			</ol>
						  			<br>
						  			<br>
						  			<a class="button" href="<?php echo base_url('sign-up')?>">Try now!</a>
						  		</div>
						  	</div>
				  		</div>
				  	</div>

				  	<!-- slide4 -->
				  	<div class="item   ">						  		
				  		<div class="slide1">
				  			<div class="container">						  			
					  			<div class="col-md-6">
					  				<h1>Our user says</h1>						  				
						  			<p class="comment">
						  				I have been taught English by those teachers who are in E-way Better English
										 for more than a year. They are good at teaching and talking, futhermore having
										 a good cariculum. I have experienced lessons at about ten schools including
										 trial lessons and I talked with more than 350 teachers in a year. 
										<br>
										<br>
										Teachers of E-way Better English are so great that I can get 825 of 
										TOIEC score and can lead a meeting in English among native speakers after
										 being taught for a year. I will keep studying in E-way Better English!											
						  			</p>
						  			<b class="author">Sen Ueno,</b>
						  			<br>
						  			<br>
						  			<br>
						  			<a class="button" href="<?php echo base_url('sign-up')?>">Try now!</a>
					  			</div>
				  			</div>
				  		</div>
				  		
				  	</div>
				  	
				  	
				 

				  </div>
				  
			  	  <a class="left carousel-control" href="#carousel-slider" data-slide="prev">
				    <img src="<?php echo base_url('assets/external/img/left-arrow.png');?>" class ="img-responsive" alt="...">
				  </a>
				  <a class="right carousel-control" href="#carousel-slider" data-slide="next">
				    <img src="<?php echo base_url('assets/external/img/right-arrow.png');?>" class ="img-responsive" alt="...">
				  </a>	
				  
				</div>
		</div>
	</div>

	<div class="features-wrapper">
		<div class="container">
			<div class="row">
				<div class="features feature-1">
					<h3>Access E-way anywhere</h3>
					<div> <img src="<?php echo base_url('assets/external/img/cloud.png');?>" class ="img-responsive" alt="..."></div>
					<hr>
					<ul>						
						<li>Access E-way and have</li>
						<li>your classess in a PC,</li>
						<li> ipad, Android devices</li>
						<li>and laptops.</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
					</ul> 
				
				</div>
				<div class="features feature-2">
					<h3>Organized Curriculum</h3>
					<div> <img src="<?php echo base_url('assets/external/img/round.png');?>" class ="img-responsive" alt="..."></div>
					<hr>
					<ul>						
						<li>With three specific</li>
						<li>categories of classes,</li>
						<li>E-way provides each</li>
						<li>client with a curriculum</li>
						<li>specially designed for</li>
						<li>your own English level!</li>
					</ul>
				</div>
				<div class="features feature-3">
					<h3>Moneyback Guarantee</h3>
					<div> <img src="<?php echo base_url('assets/external/img/coins.png');?>" class ="img-responsive" alt="..."></div>
					<hr>
					<ul>						
						<li>E-way provides innovative</li>
						<li>and excellent learning</li>
						<li>experience to its valued</li>
						<li>customers. Otherwise, you</li>
						<li>can take your money back!</li>
						<li><a class="readmore" href="#">read more</a></li>
					</ul>

				</div>
				<div class="features feature-4">
					<h3>Fast</h3>
					<div> <img src="<?php echo base_url('assets/external/img/lightning.png');?>" class ="img-responsive" alt="..."></div>
					<hr>
					<ul>						
						<li>So long as you</li>
						<li>have internet access,</li>
						<li>E-way Better English</li>
						<li>is just a click away!</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
					</ul>

				</div>

			</div>
		</div>
	</div>

	<div class="video-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="video-container">
						<div class="video"></div>
					</div>
					<p class="video-caption">
						some text here about the vide or something catchyyyyyy. this 
						is the teacher a video from teachers.
					</p>
				</div>
				<div class="col-md-6">
					<div class="dialog-container">
						<div class="first-qoute">
							<div class="last-qoute">
								<p>a perfect line from this video..</p>
							</div>
						</div>						
					</div>
					<div class="arrow">
						<p>Title of the video here.</p>
					</div>
					<div class="try-container">
						<a href="<?php echo base_url('sign-up')?>">go check it try now!</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="how-it-works-wrapper">
		<div class="container">
			<div class="row ">
				<h2>How does it work?</h2>
			</div>
			<div class="row convo-container">
				
			</div>
		</div>
	</div>

	<div class="teachers-wrapper">
		<div class="container">
			<div class="row ">
				<h2>Meet the Instructors</h2>
				<a id="teachers"></a>
			</div>
			<?php
				$profile = teacher_info(2);
				$profile = $profile[0];
				// printA($profile);profile_image
			?>
			<div class="row profile">
				<div class="col-md-3 col-md-offset-1">
					<div class="profile-container">	
						<img id="tc_prof_img" style="height: 228px;width: 231px;" src="<?php echo base_url('assets/avatars/teachers/'.$profile->profile_image);?>" class ="img-responsive" alt="...">		
					</div>
				</div>
				<div class="col-md-6" id="teachers-slider">
					<table >
						<tr>
							<td>Teaching Experience:</td>
							<td><span id="exp"><?php echo calculate_work_experience($profile->start_teaching);?></span>  english teacher</td>
						</tr>
						<tr>
							<td>Educational Background:</td>
							<td id="ed_bg"><?php echo $profile->educational_background;?></td>
						</tr>
						
					</table>
					<p id="tc_motto"><?php echo $profile->motto;?></p>
				</div>
				<div class="clearfix"></div>
				<?php
					$teachers = teacher_info();
					//printA($teachers);
				?>
				<div class="teacher-slider">					
				  <ul id="mycarousel" class="jcarousel-skin-tango">
				  	<?php
				  		foreach($teachers as $teacher){
				  	?>
				    <li>
				    	<div class="carousel-profile">
				    		<div class="profile-img">
								<a href="javascript: void(0);" onclick="javascript: teacher_selected(<?php echo $teacher->id;?>)">
									<img src="<?php echo base_url('assets/avatars/teachers/'.$teacher->profile_image);?>">
								</a>					    			
				    		</div>
				    		<p><?php echo $teacher->firstname.' '.$teacher->lastname;?></p>
				    	</div>
				    </li>
				    <?php
				    	}
				    ?>
				    
				  </ul>
				</div>
			</div>
		</div> 
	</div>

	<div class="pricing-wrapper">
		<div class="container">
			
			<div class="row">
				<div class="col-md-4 pull-left ">
					<div class="trial-container">
						<div class="btn-trial">
							<a href="<?php echo base_url('sign-up')?>">try it now!</a>
						</div>
						
						<p class="from">from</p>
						<img class="image-responsive" src="<?php  echo base_url('assets/external/img/trial-oblong.png');?>">
						<p class="free">free trial</p>
					</div>
					
				</div>
				<div class="materials-container">
					<div class="col-md-2"><img class="image-responsive children" src="<?php echo base_url('assets/external/img/children-material.png');?>"></div>
					<div class="col-md-2"><img class="image-responsive" src="<?php echo base_url('assets/external/img/adult-material.png');?>"></div>
					<div class="col-md-2"><img class="image-responsive biz" src="<?php echo base_url('assets/external/img/biz-material.png');?>"></div>
				</div>
				
				
			</div>
		</div>
		<div class=" blue-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-4">

						<div class="dialog-container">
							<div class="first-qoute">
								<div class="last-qoute">
									<p>
										or <a href="<?php echo base_url('sign-up')?>">enroll</a> to unlock full access of all materials, with 
										one on one classes from your favorite teacher.
									</p>
									<div class="btn-cont">										
										<a class="btn" href="<?php echo base_url('pricing')?>">learn the pricing</a>
									</div>
								</div>
							</div>						
						</div>

					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="footer-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<?php
						$form_attr = array('name'=>'feedback_form');
						 echo form_open('external/public_feedback',$form_attr);  ?>
						<p>contact us</p>
						<label>email</label><br>
						<input type="text" name="email"><br>
						<label>name</label><br>
						<input type="text" name="uname"><br>
						<label>message</label><br>
						<textarea rows="7" name="message"></textarea><br>
						<button>send</button>
					<?php echo form_close(); ?>
					<div id="feedbackerror"></div>
				</div>
				<div class="col-md-6 cont-comment">
					<div class="messge-box">
						<p>enjoy learning with us!</p>
					</div>
					<div class="arrow"></div>
					<div class="feedback"><a href="<?php echo base_url('sign-up')?>">try now</a></div>
				</div>
				<div class="col-md-3 ">
					<div class="feeds">
						<table>
							<?php echo $notify?>
						</table>
					</div>
					<div class="social-links">
						<div class="icons">
							<img class="image-responsive " src="<?php echo base_url('assets/external/img/twitter.png');?>">
							<img class="image-responsive " src="<?php echo base_url('assets/external/img/facebook.png');?>">
							<img class="image-responsive " src="<?php echo base_url('assets/external/img/skype.png');?>">
							<img class="image-responsive " src="<?php echo base_url('assets/external/img/google.png');?>">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

