<!DOCTYPE html>
<html>
<head>
	<title>Landing Page</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap3/dist/css/bootstrap.min.css');?>">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/external/bootstrap3/dist/css/bootstrap-theme.css');?>"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/external/bootstrap3/dist/css/bootstrap.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/external/css/custom.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/jcarousel/skins/tango/skin.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/external/css/datepicker.css');?>">
	

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="<?php echo base_url('assets/external/bootstrap3/assets/js/html5shiv.js');?>"></script>
	  <script src="<?php echo base_url('assets/external/bootstrap3/assets/js/respond.min.js');?>"></script>
	<![endif]-->
</head>
<body>
	<div class="header-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-3 logo">
					<a href="<?php echo base_url()?>"><img class="image-responsive" src="<?php echo base_url('assets/external/img/logo.png');?>"></a>
				</div>
				<div class="col-md-6 navigation">
					<ul>
						<li><a href="<?php echo base_url()?>">home</a></li>
						<li><a href="<?php echo base_url()?>#teachers">teachers</a></li>
						<li><a href="<?php echo base_url('materials')?>">materials</a></li>
						<li><a href="<?php echo base_url('pricing')?>">rates &amp; payments</a></li>
						<li><a href="<?php echo base_url('aboutus')?>">about us</a></li>
						<li><a href="<?php echo base_url('faqs')?>">FAQs</a></li>
					</ul>
				</div>
				<div class="col-md-3 pull-right cta-buttons">
					<a href="<?php echo base_url('sign-up')?>" class="try">try now!</a>
					<a href="#<?php //echo base_url('login')?>" id="sign-in" class="login">sign in</a>
					<div class="login-popdown " id="login-dropdown" style="display: none">
						<div class="login-wrapper">
							
								<?php 
									$form_attr = array('name'=>'signup_form');
									echo form_open('validate',$form_attr);
								?>

								<p><img src="<?php echo base_url('assets/external/img/case.png');?>"><span>sign in </span></p><br/>
								
								<div id="loginerror" style="position: absolute;margin-top: -20px;width: 87%;"></div>
								<label>username</label><br>
								<input type="text" name="username" value="<?php echo set_value('username');?>"><br>
								<label>password</label><br>
								<input type="password" name="password"><br>
								<a href="#">forgot password?</a><br>
								<button name="login_submit" type="submit">login</button>
								</form>
							
							<hr></hr>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php echo $contents;?>


	<div class="footer-links">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<p class="copy">&copy; ewayenglish.com</p>					
				</div>
				<div class="col-md-6">
					<ul class="navigation">
						<li><a href="<?php echo base_url()?>">home</a></li>
						<li><a href="<?php echo base_url()?>#teachers">teachers</a></li>
						<li><a href="<?php echo base_url('materials')?>">materials</a></li>
						<li><a href="<?php echo base_url('pricing')?>">rates &amp; payments</a></li>
						<li><a href="<?php echo base_url('aboutus')?>">about us</a></li>
						<li><a href="<?php echo base_url('faqs')?>">FAQs</a></li>
					</ul>
				</div>
				<?php if($this->router->fetch_method()!='index'):?>
				<div class="col-md-3">
					<div class="icons">
						<img class="image-responsive " src="<?php echo base_url('assets/external/img/twitter.png');?>">
						<img class="image-responsive " src="<?php echo base_url('assets/external/img/facebook.png');?>">
						<img class="image-responsive " src="<?php echo base_url('assets/external/img/skype.png');?>">
						<img class="image-responsive " src="<?php echo base_url('assets/external/img/google.png');?>">
					</div>
				</div>
			<?php endif;?>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="<?php echo base_url('assets/external/js/jquery.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap3/dist/js/bootstrap.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/jcarousel/lib/jquery.jcarousel.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/external/js/custom.js');?>"></script>	
	<script type="text/javascript" src="<?php echo base_url('assets/external/js/bootstrap-datepicker.js');?>"></script>
	<script type="text/javascript">

	jQuery(document).ready(function() {
		jQuery('#mycarousel').jcarousel();
		jQuery('#datepicker').datepicker();
	});


	</script>
</body>
</html>