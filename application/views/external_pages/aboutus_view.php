<div class="about-us-top">
	<div class="container">
		<div class="row">
			<h1>About Us</h1>
				<div class="col-md-10 col-md-offset-1">
					<div class="first">
						<img src="<?php echo base_url('assets/external/img/about-us-globe.png');?>">
						<p>
							E-Way Better English Language Institute is a <span>dynamic</span> and <br/>
							<span>innovative</span> learning institution that caters to<br/>
							English language learners from different nationalities.<br/>
						</p>
					</div>
					<div class="second">
						<img src="<?php echo base_url('assets/external/img/about-us-grad.png');?>">
						<p>					
							The institution combines specialized knowledge <br/>
							by <span>education</span> and expertise by <span>profession</span> of the Instructors in ESL teaching industry. <br/>
							The company uses only materials that are <span>standardized</span> <br/>
							according to the way the English language is taught in Philippine universities.<br/>
						</p>
						
					</div>
					<div class="third">		
						<img src="<?php echo base_url('assets/external/img/about-us-target.png');?>">				
						<p>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							E-Way Better English Language Institute (EBELI) <br/>
						aims to incorporate the <span>core values</span> and learning <span>practices</span><br/>
						 &nbsp;&nbsp;of the Philippine academe’s English education system into<br/>
						 &nbsp;&nbsp;the online ESL industry.<br/>
						</p>
						
					</div>
			</div>
		</div>
	</div>
</div>

<div class="about-us-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h1>Mission/Vision of EBELI</h1>
				<p class="tag"><span>"</span>&nbsp;&nbsp;Diversity is the one true thing we all have in common- celebrate it today &nbsp;&nbsp;<span>"</span></p>
				<h2>Corporate Vision:</h2>
				<p class="dat">
					E-Way Better English shall be recognized as an innovative leader in 
					providing opportunities for English Language learners to acquire and develop
					 competency in the English Language through sophisticated, entertaining and 
					dynamic learning experience.
				</p>
				<h2>Corporate Mission:</h2>
				<p class="dat">
					Being an innovative leader in ESL teaching industry, e-Way Better English
					Language Institute (EBELI) strives to provide excellent learning experience to
					each client through sophisticated system of ESL teaching. 
				</p>
				<p class="dat">
					Through the use of standardized materials specifically crafted by Linguistics
					professionals, it is our mission to incorporate the English education system of 
					the Philippine academe into the online ESL teaching industry.
				</p>
			</div>
		</div>
	</div>
</div>
